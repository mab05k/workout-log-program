﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WorkoutProgram
{
    /// <summary>
    /// Interaction logic for WorkoutScore.xaml
    /// </summary>
    public partial class WorkoutScore : Page
    {
        public string excelDBSource;

        public string selectedWorkoutName;
        public string workoutNameSelectedValue;
        public string workoutTypeInt;
        public Border displayBorder;
        public DataTable workoutNameDataTable;
        public ComboBox comboBoxWorkoutScoreName;
        public TextBox textBoxWorkoutScoreTimeMinutes;
        public TextBox textBoxWorkoutScoreTimeSeconds;
        public TextBox textBoxWorkoutDate;
        public Calendar calendarWorkoutDate;
        public Grid infoGrid;
        public Label timeLabel;

        public WorkoutScore(string ExcelDBSource)
        {
            try
            {
                InitializeComponent();
                excelDBSource = ExcelDBSource;
                #region Main Menu labels/combobox/button
                Label labelWorkoutScoreName = new Label();
                labelWorkoutScoreName.Content = "Workout Name:";
                labelWorkoutScoreName.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
                labelWorkoutScoreName.HorizontalAlignment = HorizontalAlignment.Left;
                labelWorkoutScoreName.VerticalAlignment = VerticalAlignment.Top;
                labelWorkoutScoreName.Margin = new Thickness(10, 20, 0, 0);
                inputGrid.Children.Add(labelWorkoutScoreName);

                ComboBox cb = new ComboBox();
                cb.HorizontalAlignment = HorizontalAlignment.Left;
                cb.VerticalAlignment = VerticalAlignment.Top;
                cb.Width = 120;
                cb.Margin = new Thickness(110, 25, 0, 0);
                cb.DropDownClosed += OnComboBoxWorkoutScoreName_DropdownClosed;
                comboBoxWorkoutScoreName = cb;
                inputGrid.Children.Add(comboBoxWorkoutScoreName);

                Button b = new Button();
                b.Content = "Add";
                b.HorizontalAlignment = HorizontalAlignment.Left;
                b.VerticalAlignment = VerticalAlignment.Top;
                b.Margin = new Thickness(235, 25, 0, 0);
                b.Width = 70;
                b.Height = Double.NaN;
                b.Style = (Style)FindResource("newScoreButton");
                b.Click += OnButtonAddScore_Click;
                inputGrid.Children.Add(b);
                #endregion

                using (OleDbConnection conn = new OleDbConnection(excelDBSource))
                {
                    conn.Open();
                    string select = "SELECT * FROM [WorkoutName$] WHERE Id > 0 ORDER BY WorkoutName";
                    OleDbCommand cmd = new OleDbCommand(select, conn);

                    OleDbDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    workoutNameDataTable = dt;

                    comboBoxWorkoutScoreName.ItemsSource = dt.DefaultView;
                    comboBoxWorkoutScoreName.DisplayMemberPath = "WorkoutName";
                    comboBoxWorkoutScoreName.SelectedValuePath = "Id";
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Critical Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(3);
            }
        }

        private string DisplayWorkout(string message)
        {         
            string theWorkout = null;   
            if (comboBoxWorkoutScoreName.Text == "")
            {
                return theWorkout;
            }
            else
            {
                string selectedValue = comboBoxWorkoutScoreName.SelectedValue.ToString();
                workoutNameSelectedValue = selectedValue;

                using (OleDbConnection conn = new OleDbConnection(excelDBSource))
                {
                    conn.Open();                    

                    #region lists
                    List<string> name = new List<string>();
                    List<string> type = new List<string>();
                    List<string> amrapDuration = new List<string>();
                    List<string> exercises = new List<string>();
                    List<string> exerciseTypeID = new List<string>();
                    List<string> weight = new List<string>();
                    List<string> reps = new List<string>();
                    List<string> dist = new List<string>();
                    name.Clear();
                    type.Clear();
                    amrapDuration.Clear();
                    exercises.Clear();
                    exerciseTypeID.Clear();
                    weight.Clear();
                    reps.Clear();
                    dist.Clear();
                    #endregion

                    #region db query strings

                    string workoutName = string.Format("SELECT * FROM [WorkoutName$] WHERE Id = {0}", selectedValue);
                    string workoutType = string.Format("SELECT * FROM [WorkoutType$]");
                    string exerciseWRD = string.Format("SELECT * FROM [ExerciseWeightRepDist$] WHERE WorkoutName_ID = {0}", selectedValue);
                    string exerciseType = string.Format("SELECT * FROM [ExerciseType$]");

                    #endregion

                    #region DataSet creation / fill
                    DataSet ds = new DataSet();
                    OleDbDataAdapter wName = new OleDbDataAdapter(workoutName, conn);
                    OleDbDataAdapter wType = new OleDbDataAdapter(workoutType, conn);
                    OleDbDataAdapter eWRD = new OleDbDataAdapter(exerciseWRD, conn);
                    OleDbDataAdapter eType = new OleDbDataAdapter(exerciseType, conn);

                    DataTable wNameDataTable = new DataTable();
                    DataTable wTypeDataTable = new DataTable();
                    DataTable eWRDDataTable = new DataTable();
                    DataTable eTypeDataTable = new DataTable();

                    wName.Fill(wNameDataTable);
                    wType.Fill(wTypeDataTable);
                    eWRD.Fill(eWRDDataTable);
                    eType.Fill(eTypeDataTable);

                    ds.Tables.Add(wNameDataTable);
                    ds.Tables.Add(wTypeDataTable);
                    ds.Tables.Add(eWRDDataTable);
                    ds.Tables.Add(eTypeDataTable);
                    #endregion

                    #region populate workoutName + amrapDuration
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        name.Add(ds.Tables[0].Rows[i]["WorkoutName"].ToString());
                        amrapDuration.Add(ds.Tables[0].Rows[i]["amrapDuration"].ToString());

                        selectedWorkoutName = ds.Tables[0].Rows[i]["WorkoutName"].ToString();
                        workoutTypeInt = ds.Tables[0].Rows[i]["WorkoutType_ID"].ToString();
                    }
                    #endregion

                    #region populate workoutType
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {
                        if (ds.Tables[1].Rows[i]["Id"].ToString() == workoutTypeInt)
                        {
                            type.Add(ds.Tables[1].Rows[i]["WorkoutTypeName"].ToString());
                        }
                    }
                    #endregion

                    #region populate exercise weight, reps, dist
                    for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                    {
                        if (ds.Tables[2].Rows[i]["WorkoutName_ID"].ToString() == workoutNameSelectedValue)
                        {
                            weight.Add(ds.Tables[2].Rows[i]["ExerciseWeight"].ToString());
                            reps.Add(ds.Tables[2].Rows[i]["ExerciseReps"].ToString());
                            dist.Add(ds.Tables[2].Rows[i]["ExerciseDist"].ToString());
                            exerciseTypeID.Add(ds.Tables[2].Rows[i]["ExerciseType_ID"].ToString());
                        }
                    }
                    #endregion

                    #region populate exercises
                    for (int i = 0; i < ds.Tables[3].Rows.Count; i++)
                    {
                        foreach (var exercise in exerciseTypeID)
                        {
                            if (ds.Tables[3].Rows[i]["Id"].ToString() == exercise)
                            {
                                exercises.Add(ds.Tables[3].Rows[i]["ExerciseTypeName"].ToString());
                            }
                        }

                    }
                    #endregion

                    #region display workout
                    for (int i = 0; i < name.Count; i++)
                    {
                        if (amrapDuration[i] == "")
                            theWorkout += string.Format("\"{0}\"\n{1}\n", name[i], type[i]);
                        else
                            theWorkout += string.Format("\"{0}\"\n{1}: {2} minutes\n", name[i], type[i], amrapDuration[i]);
                    }

                    for (int i = 0; i < exercises.Count; i++)
                    {
                        if (dist[i] != "")
                        {
                            theWorkout += string.Format("{0} {1} m\n", exercises[i], dist[i]);
                        }

                        if (weight[i] != "")
                        {
                            if (reps[i] != "")
                                theWorkout += string.Format("{0}# {1}, {2} reps\n", weight[i], exercises[i], reps[i]);
                            else
                                theWorkout += string.Format("{0}# {1}\n", weight[i], exercises[i]);
                        }

                        if (reps[i] != "" && weight[i] == "")
                        {
                            theWorkout += string.Format("{0}, {1} reps\n", exercises[i], reps[i]);
                        }

                        if (weight[i] == "" && reps[i] == "" && dist[i] == "")
                            theWorkout += string.Format("{0}\n", exercises[i]);
                    }
                    #endregion
                    return theWorkout;
                }
            }
        }

        private void OnComboBoxWorkoutScoreName_DropdownClosed(object sender, EventArgs e)
        {
            if (comboBoxWorkoutScoreName.Text == "" || comboBoxWorkoutScoreName.Text == " ")
            {
                wrapPanelDisplayWorkout.Children.Remove(displayBorder); // Removes all workout displays
            }
            else
            {
                using (OleDbConnection conn = new OleDbConnection(excelDBSource))
                {
                    conn.Open();
                    inputGrid.Children.Remove(infoGrid);
                    ShowWorkoutScoreHistory(conn);
                    CreateScoreInput(conn);                    
                }
                textBoxWorkoutScoreTimeMinutes.Focus();
            }
            
        }
        private void ShowWorkoutScoreHistory(OleDbConnection conn)
        {
            #region Code for Displaying Score History
                string message = null;
                string selectedValue = comboBoxWorkoutScoreName.SelectedValue.ToString();
                workoutNameSelectedValue = selectedValue;

                wrapPanelDisplayWorkout.Children.Remove(displayBorder);

                #region Fill DataTable
                string workoutScore = string.Format("SELECT * " +
                                                   "FROM [WorkoutScore$] WHERE WorkoutName_ID = {0}", workoutNameSelectedValue);
                DataSet data = new DataSet();
                OleDbDataAdapter wScore = new OleDbDataAdapter(workoutScore, conn);
                DataTable wScoreDataTable = new DataTable();
                wScore.Fill(wScoreDataTable);
                data.Tables.Add(wScoreDataTable);
                #endregion

                if (wScoreDataTable.Rows.Count == 0)
                {

                }
                else
                {

                    StackPanel sp = new StackPanel();

                    #region TextBlock
                    TextBlock tb = new TextBlock();
                    tb.HorizontalAlignment = HorizontalAlignment.Left;
                    tb.VerticalAlignment = VerticalAlignment.Top;
                    tb.TextWrapping = TextWrapping.Wrap;
                    tb.Width = 120;
                    tb.MinWidth = 100;
                    tb.Height = Double.NaN;
                    tb.FontFamily = new FontFamily("Verdana");
                    tb.FontSize = 10;
                    tb.Background = Brushes.Transparent;
                    tb.Visibility = Visibility.Visible;
                    tb.Padding = new Thickness(10, 10, 10, 0);
                    tb.Margin = new Thickness(0, 5, 0, 0);
                    tb.Text = DisplayWorkout(message);
                    #endregion

                    #region Border
                    Border b = new Border();
                    b.BorderBrush = Brushes.Black;
                    b.Background = new SolidColorBrush(Color.FromRgb(224, 224, 224));
                    b.Height = Double.NaN;
                    b.Width = Double.NaN;
                    b.BorderThickness = new Thickness(0, 0, 0, 0);
                    b.Padding = new Thickness(5, 0, 5, 10);
                    b.Margin = new Thickness(20, 30, 0, 10);
                    #endregion

                    #region ListView
                    ListView lv = new ListView();
                    lv.HorizontalAlignment = HorizontalAlignment.Left;
                    lv.VerticalAlignment = VerticalAlignment.Top;
                    lv.MinWidth = 100;
                    lv.Width = Double.NaN;
                    lv.Height = Double.NaN;
                    lv.IsHitTestVisible = false;
                    lv.FontFamily = new FontFamily("Verdana");
                    lv.FontSize = 10;
                    lv.Foreground = Brushes.Black;
                    lv.Background = Brushes.White;
                    lv.Padding = new Thickness(0);
                    lv.Margin = new Thickness(5, 1, 5, 0);
                    lv.BorderBrush = Brushes.Black;
                    lv.BorderThickness = new Thickness(1);
                    lv.Visibility = Visibility.Visible;
                    #endregion

                    #region GridView
                    GridView myGridView = new GridView();
                    myGridView.AllowsColumnReorder = true;
                    myGridView.ColumnHeaderToolTip = "test";

                    GridViewColumn date = new GridViewColumn();
                    date.Header = "Date";
                    date.DisplayMemberBinding = new Binding("WorkoutDate");
                    date.DisplayMemberBinding.StringFormat = "{0:dd MMM yy}";
                    date.Width = Double.NaN;
                    myGridView.Columns.Add(date);

                    GridViewColumn gScore = new GridViewColumn();
                    gScore.Header = "Score";
                    if (data.Tables[0].Rows[0].ItemArray[3].ToString() != "")
                        gScore.DisplayMemberBinding = new Binding("WorkoutScoreTime") { Converter = new Converters() };
                    if (data.Tables[0].Rows[0].ItemArray[4].ToString() != "")
                        gScore.DisplayMemberBinding = new Binding("WorkoutScoreReps");
                    if (data.Tables[0].Rows[0].ItemArray[5].ToString() != "")
                        gScore.DisplayMemberBinding = new Binding("WorkoutScoreWeight");
                    gScore.Width = Double.NaN;
                    myGridView.Columns.Add(gScore);
                    #endregion

                    #region Display Data
                    lv.View = myGridView;
                    lv.ItemsSource = data.Tables[0].DefaultView;

                    displayBorder = b;
                    b.Child = sp;
                    sp.Children.Add(tb);
                    sp.Children.Add(lv);

                    wrapPanelDisplayWorkout.Children.Add(b);
                    #endregion
                }
            #endregion
        }
        private void CreateScoreInput(OleDbConnection conn)
        {
                string workoutType = string.Format("SELECT * FROM [WorkoutName$] WHERE Id = {0}", workoutNameSelectedValue);

                OleDbDataAdapter wType = new OleDbDataAdapter(workoutType, conn);
                DataTable wTypeDataTable = new DataTable();
                wType.Fill(wTypeDataTable);

                Grid InfoGrid = new Grid();
                inputGrid.Children.Add(InfoGrid);
                infoGrid = InfoGrid;

                #region IF workout requires a "Time" score entry
                if (wTypeDataTable.Rows[0].ItemArray[2].ToString() != "2" && wTypeDataTable.Rows[0].ItemArray[2].ToString() != "6")
                {
                    #region Labels
                    Label TimeLabel = new Label();
                    TimeLabel.Content = "Score:";
                    TimeLabel.HorizontalAlignment = HorizontalAlignment.Left;
                    TimeLabel.VerticalAlignment = VerticalAlignment.Top;
                    TimeLabel.Visibility = Visibility.Visible;
                    TimeLabel.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
                    TimeLabel.Margin = new Thickness(65, 52, 0, 0);

                    Label colon = new Label();
                    colon.Content = ":";
                    colon.HorizontalAlignment = HorizontalAlignment.Left;
                    colon.VerticalAlignment = VerticalAlignment.Top;
                    colon.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
                    colon.Margin = new Thickness(131, 53, 0, 0);

                    Label minutes = new Label();
                    minutes.Content = "Min";
                    minutes.HorizontalAlignment = HorizontalAlignment.Left;
                    minutes.VerticalAlignment = VerticalAlignment.Top;
                    minutes.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
                    minutes.Margin = new Thickness(114, 44, 0, 0);
                    minutes.FontSize = 8;

                    Label seconds = new Label();
                    seconds.Content = "Sec";
                    seconds.HorizontalAlignment = HorizontalAlignment.Left;
                    seconds.VerticalAlignment = VerticalAlignment.Top;
                    seconds.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
                    seconds.Margin = new Thickness(145, 44, 0, 0);
                    seconds.FontSize = 8;
                    #endregion

                    #region TextBoxes

                    TextBox TextBoxWorkoutScoreTimeMinutes = new TextBox();
                    TextBoxWorkoutScoreTimeMinutes.Text = "0";
                    TextBoxWorkoutScoreTimeMinutes.HorizontalAlignment = HorizontalAlignment.Left;
                    TextBoxWorkoutScoreTimeMinutes.HorizontalContentAlignment = HorizontalAlignment.Right;
                    TextBoxWorkoutScoreTimeMinutes.VerticalAlignment = VerticalAlignment.Top;
                    TextBoxWorkoutScoreTimeMinutes.Margin = new Thickness(110, 60, 0, 0);
                    TextBoxWorkoutScoreTimeMinutes.Width = 25;
                    TextBoxWorkoutScoreTimeMinutes.GotFocus += OnTextBoxMinutes_GotFocus;

                    TextBox TextBoxWorkoutScoreTimeSeconds = new TextBox();
                    TextBoxWorkoutScoreTimeSeconds.Text = "00";
                    TextBoxWorkoutScoreTimeSeconds.HorizontalAlignment = HorizontalAlignment.Left;
                    TextBoxWorkoutScoreTimeSeconds.HorizontalContentAlignment = HorizontalAlignment.Right;
                    TextBoxWorkoutScoreTimeSeconds.VerticalAlignment = VerticalAlignment.Top;
                    TextBoxWorkoutScoreTimeSeconds.Margin = new Thickness(140, 60, 0, 0);
                    TextBoxWorkoutScoreTimeSeconds.Width = 25;
                    TextBoxWorkoutScoreTimeSeconds.GotFocus += OnTextBoxSeconds_GotFocus;

                    #endregion

                    textBoxWorkoutScoreTimeMinutes = TextBoxWorkoutScoreTimeMinutes;
                    textBoxWorkoutScoreTimeSeconds = TextBoxWorkoutScoreTimeSeconds;
                    timeLabel = TimeLabel;
                    infoGrid.Children.Add(TimeLabel);
                    infoGrid.Children.Add(colon);
                    infoGrid.Children.Add(minutes);
                    infoGrid.Children.Add(seconds);
                    infoGrid.Children.Add(TextBoxWorkoutScoreTimeMinutes);
                    infoGrid.Children.Add(TextBoxWorkoutScoreTimeSeconds);
                }
                #endregion

                #region IF workout requires a "Weight" score entry
                if (wTypeDataTable.Rows[0].ItemArray[2].ToString() == "6")
                {
                    Label TimeLabel = new Label();
                    TimeLabel.Content = "Weight:";
                    TimeLabel.HorizontalAlignment = HorizontalAlignment.Left;
                    TimeLabel.VerticalAlignment = VerticalAlignment.Top;
                    TimeLabel.Visibility = Visibility.Visible;
                    TimeLabel.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
                    TimeLabel.Margin = new Thickness(53, 52, 0, 0);

                    Label weight = new Label();
                    weight.Content = "#";
                    weight.HorizontalAlignment = HorizontalAlignment.Left;
                    weight.VerticalAlignment = VerticalAlignment.Top;
                    weight.Visibility = Visibility.Visible;
                    weight.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
                    weight.Margin = new Thickness(132, 54, 0, 0);

                    TextBox TextBoxWorkoutScoreTimeMinutes = new TextBox();
                    TextBoxWorkoutScoreTimeMinutes.Text = "0";
                    TextBoxWorkoutScoreTimeMinutes.HorizontalAlignment = HorizontalAlignment.Left;
                    TextBoxWorkoutScoreTimeMinutes.HorizontalContentAlignment = HorizontalAlignment.Right;
                    TextBoxWorkoutScoreTimeMinutes.VerticalAlignment = VerticalAlignment.Top;
                    TextBoxWorkoutScoreTimeMinutes.Margin = new Thickness(110, 60, 0, 0);
                    TextBoxWorkoutScoreTimeMinutes.Width = 25;
                    TextBoxWorkoutScoreTimeMinutes.Visibility = Visibility.Visible;
                    TextBoxWorkoutScoreTimeMinutes.GotFocus += OnTextBoxMinutes_GotFocus;

                    textBoxWorkoutScoreTimeMinutes = TextBoxWorkoutScoreTimeMinutes;
                    timeLabel = TimeLabel;
                    infoGrid.Children.Add(TimeLabel);
                    infoGrid.Children.Add(weight);
                    infoGrid.Children.Add(TextBoxWorkoutScoreTimeMinutes);
                }
                #endregion

                #region IF workout requires a "Rounds" score entry
                if (wTypeDataTable.Rows[0].ItemArray[2].ToString() == "2")
                {
                    Label TimeLabel = new Label();
                    TimeLabel.Content = "Rounds:";
                    TimeLabel.HorizontalAlignment = HorizontalAlignment.Left;
                    TimeLabel.VerticalAlignment = VerticalAlignment.Top;
                    TimeLabel.Visibility = Visibility.Visible;
                    TimeLabel.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
                    TimeLabel.Margin = new Thickness(50, 52, 0, 0);

                    TextBox TextBoxWorkoutScoreTimeMinutes = new TextBox();
                    TextBoxWorkoutScoreTimeMinutes.Text = "0";
                    TextBoxWorkoutScoreTimeMinutes.HorizontalAlignment = HorizontalAlignment.Left;
                    TextBoxWorkoutScoreTimeMinutes.HorizontalContentAlignment = HorizontalAlignment.Right;
                    TextBoxWorkoutScoreTimeMinutes.VerticalAlignment = VerticalAlignment.Top;
                    TextBoxWorkoutScoreTimeMinutes.Margin = new Thickness(110, 60, 0, 0);
                    TextBoxWorkoutScoreTimeMinutes.Width = 25;
                    TextBoxWorkoutScoreTimeMinutes.Visibility = Visibility.Visible;
                    TextBoxWorkoutScoreTimeMinutes.GotFocus += OnTextBoxMinutes_GotFocus;

                    textBoxWorkoutScoreTimeMinutes = TextBoxWorkoutScoreTimeMinutes;
                    timeLabel = TimeLabel;
                    infoGrid.Children.Add(TimeLabel);
                    infoGrid.Children.Add(TextBoxWorkoutScoreTimeMinutes);
                }
                #endregion

                #region Date Information
                Label dateLabel = new Label();
                dateLabel.Content = "Date:";
                dateLabel.HorizontalAlignment = HorizontalAlignment.Left;
                dateLabel.VerticalAlignment = VerticalAlignment.Top;
                dateLabel.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
                dateLabel.Margin = new Thickness(65, 80, 0, 0);

                TextBox TextBoxWorkoutDate = new TextBox();
                TextBoxWorkoutDate.Text = "";
                TextBoxWorkoutDate.HorizontalAlignment = HorizontalAlignment.Left;
                TextBoxWorkoutDate.VerticalAlignment = VerticalAlignment.Top;
                TextBoxWorkoutDate.Height = 23;
                TextBoxWorkoutDate.Width = 120;
                TextBoxWorkoutDate.Margin = new Thickness(110, 85, 0, 0);
                TextBoxWorkoutDate.TextWrapping = TextWrapping.Wrap;
                TextBoxWorkoutDate.IsReadOnly = true;
                TextBoxWorkoutDate.GotFocus += OnTextBoxWorkoutDate_GotFocus;
                textBoxWorkoutDate = TextBoxWorkoutDate;

                Calendar CalendarWorkoutDate = new Calendar();
                CalendarWorkoutDate.HorizontalAlignment = HorizontalAlignment.Left;
                CalendarWorkoutDate.VerticalAlignment = VerticalAlignment.Top;
                CalendarWorkoutDate.Visibility = Visibility.Collapsed;
                CalendarWorkoutDate.Margin = new Thickness(110, 100, 0, 0);
                CalendarWorkoutDate.SelectedDatesChanged += OnCalendarWorkoutDate_SelectionChanged;
                calendarWorkoutDate = CalendarWorkoutDate;

                infoGrid.Children.Add(dateLabel);
                infoGrid.Children.Add(TextBoxWorkoutDate);
                infoGrid.Children.Add(CalendarWorkoutDate);
                #endregion
        }

        private void OnTextBoxWorkoutDate_GotFocus(object sender, RoutedEventArgs e)
        {
            calendarWorkoutDate.Visibility = Visibility.Visible;
        }
        private void OnCalendarWorkoutDate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime myTime = (DateTime)calendarWorkoutDate.SelectedDate;
            textBoxWorkoutDate.Text = myTime.Date.ToString("dd MMM yyyy");
            calendarWorkoutDate.Visibility = Visibility.Hidden;
        }
        private void OnTextBoxMinutes_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxWorkoutScoreTimeMinutes.SelectAll();
        }
        private void OnTextBoxSeconds_GotFocus(object sender, RoutedEventArgs e)
        {
            textBoxWorkoutScoreTimeSeconds.SelectAll();
        }

        private void OnButtonAddScore_Click(object sender, RoutedEventArgs e)
        {
            try
            {            
            int errorCount = 0;
            string errorMessage = "";

            #region Input Parameters
            // Calculate workoutTime Format in seconds
            int totalTime = 0;
            if ((string)timeLabel.Content == "Score:")
            {
                int minutesToSeconds = int.Parse(textBoxWorkoutScoreTimeMinutes.Text) * 60;
                int Seconds = int.Parse(textBoxWorkoutScoreTimeSeconds.Text);
                totalTime = minutesToSeconds + Seconds;
            }

            int totalWeight = 0;
            if ((string)timeLabel.Content == "Weight:")
            {
                totalWeight = int.Parse(textBoxWorkoutScoreTimeMinutes.Text);
            }

            int totalRounds = 0;
            if ((string)timeLabel.Content == "Rounds:")
            {
                totalRounds = int.Parse(textBoxWorkoutScoreTimeMinutes.Text);
            }
            #endregion

            #region Errors
            if (comboBoxWorkoutScoreName.Text == "")
            {
                errorMessage += "Workout not selected.\n";
                errorCount++;
            }
            if (textBoxWorkoutScoreTimeMinutes.Text == "0" || textBoxWorkoutScoreTimeMinutes.Text == "") // need error catching if this is not created yet..
            {
                if (timeLabel.Content == "Score:")
                {
                    errorMessage += "Must input a value for minutes.\n";
                    errorCount++;
                }
                if (timeLabel.Content == "Weight:")
                {
                    errorMessage += "Must input a weight.\n";
                    errorCount++;
                }
                if (timeLabel.Content == "Rounds:")
                {
                    errorMessage += "Must input number of rounds.\n";
                    errorCount++;
                }
            }
            if (textBoxWorkoutScoreTimeSeconds != null)
            {
                if (textBoxWorkoutScoreTimeSeconds.Text == "") // same...
                {
                    errorMessage += "Must input a value for seconds.\n";
                    errorCount++;
                }
            }
            if (calendarWorkoutDate.SelectedDate == null)
            {
                errorMessage += "Select a date for your workout.\n";
                errorCount++;
            }
            #endregion

            #region DB Add Workout
            if (errorCount == 0)
            {
                using (OleDbConnection conn = new OleDbConnection(excelDBSource))
                {
                    conn.Open();
                    string addNewScore = string.Format(@"INSERT INTO [WorkoutScore$] (WorkoutName_ID, WorkoutDate, WorkoutScoreTime, WorkoutScoreReps, WorkoutScoreWeight)
                    VALUES(@workoutName_ID, @WorkoutDate, @WorkoutScoreTime, @WorkoutScoreReps, @WorkoutScoreWeight)");

                    
                    using (OleDbCommand addScore = new OleDbCommand(addNewScore, conn))
                    {
                        addScore.Parameters.Add("@workoutName_ID", OleDbType.Integer).Value = comboBoxWorkoutScoreName.SelectedValue;
                        addScore.Parameters.Add("@WorkoutDate", OleDbType.Date).Value = calendarWorkoutDate.SelectedDate;

                        if (totalTime == 0)
                            addScore.Parameters.Add("@WorkoutScoreTime", OleDbType.Integer).Value = DBNull.Value;
                        else
                            addScore.Parameters.Add("@WorkoutScoreTime", OleDbType.Integer).Value = totalTime;

                        if (totalRounds == 0)
                            addScore.Parameters.Add("@WorkoutScoreReps", OleDbType.Integer).Value = DBNull.Value;
                        else
                            addScore.Parameters.Add("@WorkoutScoreReps", OleDbType.Integer).Value = totalRounds;

                        if (totalWeight == 0)
                            addScore.Parameters.Add("@WorkoutScoreWeight", OleDbType.Integer).Value = DBNull.Value;
                        else
                            addScore.Parameters.Add("@WorkoutScoreWeight", OleDbType.Integer).Value = totalWeight;
                                                
                        int ScoreAdded = addScore.ExecuteNonQuery();

                        string theWorkout = "New score added!\n";

                        ShowWorkoutScoreHistory(conn);
                        MessageBox.Show(theWorkout, "Added!", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    }
                    
                }
                inputGrid.Children.Remove(infoGrid);
            }
            #endregion

            if (errorCount != 0)
            {
                MessageBox.Show(errorCount + " Error(s):\n\n" + errorMessage, "Missing Information", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            }
            catch (FormatException)
            {
                MessageBox.Show("Score input must be a number.", "Invalid Input", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }            
    }
}
