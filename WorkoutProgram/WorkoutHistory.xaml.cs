﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WorkoutProgram
{
    /// <summary>
    /// Interaction logic for WorkoutHistory.xaml
    /// </summary>
    public partial class WorkoutHistory : Page
    {
        public string excelDBSource;

        public string workoutNameSelectedValue;
        public string selectedWorkoutName;        
        public string workoutTypeInt;
        public DataTable workoutDataTable;
        public DataTable scoreDataTable;
        public ScrollViewer historyScrollViewer;
        public WrapPanel historyWrapPanel;

        public WorkoutHistory(string ExcelDBSource)
        {
            try
            {
                InitializeComponent();
                excelDBSource = ExcelDBSource;
                using (OleDbConnection conn = new OleDbConnection(excelDBSource))
                {
                    conn.Open();
                    string select = "SELECT * FROM [WorkoutName$] WHERE Id > 0 ORDER BY WorkoutName ASC";
                    OleDbCommand cmd = new OleDbCommand(select, conn);

                    OleDbDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);

                    comboBoxWorkoutName.ItemsSource = dt.DefaultView;
                    comboBoxWorkoutName.DisplayMemberPath = "WorkoutName";
                    comboBoxWorkoutName.SelectedValuePath = "Id";

                    workoutDataTable = dt;

                    string workoutScore = string.Format("SELECT * FROM [WorkoutScore$]");
                    OleDbDataAdapter wScore = new OleDbDataAdapter(workoutScore, conn);
                    DataTable wScoreDataTable = new DataTable();
                    wScore.Fill(wScoreDataTable);
                    scoreDataTable = wScoreDataTable;
                }
                ShowAllWorkouts2(comboBoxWorkoutName.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Critical Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(3);
            }
        }

        private void OnComboBoxWorkoutName_DropdownClosed(object sender, EventArgs e)
        {
            #region Code for Displaying Score History

            historyDockPanel.Children.Remove(historyScrollViewer);
            historyDockPanel.Children.Remove(historyWrapPanel);

            if (comboBoxWorkoutName.Text == " " || comboBoxWorkoutName.Text == "")
            {
                ShowAllWorkouts2(comboBoxWorkoutName.Text);
            }
            else
            {
                workoutNameSelectedValue = comboBoxWorkoutName.SelectedValue.ToString();
                ShowAllWorkouts2(comboBoxWorkoutName.Text);
            }

            #endregion
        }     

        private string DisplayWorkout(string message)
        {
            string theWorkout = null;
            string selectedValue;
            if (comboBoxWorkoutName.Text == "" || comboBoxWorkoutName.Text == " ")
            {
                selectedValue = message;
                workoutNameSelectedValue = selectedValue;
            }
            else
            {
                selectedValue = comboBoxWorkoutName.SelectedValue.ToString();
                workoutNameSelectedValue = selectedValue;
            }
            using (OleDbConnection conn = new OleDbConnection(excelDBSource))
            {
                conn.Open();

                #region lists
                List<string> name = new List<string>();
                List<string> type = new List<string>();
                List<string> amrapDuration = new List<string>();
                List<string> exercises = new List<string>();
                List<string> exerciseTypeID = new List<string>();
                List<string> weight = new List<string>();
                List<string> reps = new List<string>();
                List<string> dist = new List<string>();
                name.Clear();
                type.Clear();
                amrapDuration.Clear();
                exercises.Clear();
                exerciseTypeID.Clear();
                weight.Clear();
                reps.Clear();
                dist.Clear();
                #endregion

                #region db query strings

                string workoutName = string.Format("SELECT * FROM [WorkoutName$] WHERE Id = {0}", selectedValue);
                string workoutType = string.Format("SELECT * FROM [WorkoutType$]");
                string exerciseWRD = string.Format("SELECT * FROM [ExerciseWeightRepDist$] WHERE WorkoutName_ID = {0}", selectedValue);
                string exerciseType = string.Format("SELECT * FROM [ExerciseType$]");

                #endregion

                #region DataSet creation / fill
                DataSet ds = new DataSet();
                OleDbDataAdapter wName = new OleDbDataAdapter(workoutName, conn);
                OleDbDataAdapter wType = new OleDbDataAdapter(workoutType, conn);
                OleDbDataAdapter eWRD = new OleDbDataAdapter(exerciseWRD, conn);
                OleDbDataAdapter eType = new OleDbDataAdapter(exerciseType, conn);

                DataTable wNameDataTable = new DataTable();
                DataTable wTypeDataTable = new DataTable();
                DataTable eWRDDataTable = new DataTable();
                DataTable eTypeDataTable = new DataTable();

                wName.Fill(wNameDataTable);
                wType.Fill(wTypeDataTable);
                eWRD.Fill(eWRDDataTable);
                eType.Fill(eTypeDataTable);

                ds.Tables.Add(wNameDataTable);
                ds.Tables.Add(wTypeDataTable);
                ds.Tables.Add(eWRDDataTable);
                ds.Tables.Add(eTypeDataTable);
                #endregion

                #region populate workoutName + amrapDuration
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    name.Add(ds.Tables[0].Rows[i]["WorkoutName"].ToString());
                    amrapDuration.Add(ds.Tables[0].Rows[i]["amrapDuration"].ToString());

                    selectedWorkoutName = ds.Tables[0].Rows[i]["WorkoutName"].ToString();
                    workoutTypeInt = ds.Tables[0].Rows[i]["WorkoutType_ID"].ToString();
                }
                #endregion

                #region populate workoutType
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    if (ds.Tables[1].Rows[i]["Id"].ToString() == workoutTypeInt)
                    {
                        type.Add(ds.Tables[1].Rows[i]["WorkoutTypeName"].ToString());
                    }
                }
                #endregion

                #region populate exercise weight, reps, dist
                for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                {
                    if (ds.Tables[2].Rows[i]["WorkoutName_ID"].ToString() == workoutNameSelectedValue)
                    {
                        weight.Add(ds.Tables[2].Rows[i]["ExerciseWeight"].ToString());
                        reps.Add(ds.Tables[2].Rows[i]["ExerciseReps"].ToString());
                        dist.Add(ds.Tables[2].Rows[i]["ExerciseDist"].ToString());
                        exerciseTypeID.Add(ds.Tables[2].Rows[i]["ExerciseType_ID"].ToString());
                    }
                }
                #endregion

                #region populate exercises
                for (int i = 0; i < ds.Tables[3].Rows.Count; i++)
                {
                    foreach (var exercise in exerciseTypeID)
                    {
                        if (ds.Tables[3].Rows[i]["Id"].ToString() == exercise)
                        {
                            exercises.Add(ds.Tables[3].Rows[i]["ExerciseTypeName"].ToString());
                        }
                    }

                }
                #endregion

                #region display workout
                for (int i = 0; i < name.Count; i++)
                {
                    if (amrapDuration[i] == "")
                        theWorkout += string.Format("\"{0}\"\n{1}\n", name[i], type[i]);
                    else
                        theWorkout += string.Format("\"{0}\"\n{1}: {2} minutes\n", name[i], type[i], amrapDuration[i]);
                }

                for (int i = 0; i < exercises.Count; i++)
                {
                    if (dist[i] != "")
                    {
                        theWorkout += string.Format("{0} {1} m\n", exercises[i], dist[i]);
                    }

                    if (weight[i] != "")
                    {
                        if (reps[i] != "")
                            theWorkout += string.Format("{0}# {1}, {2} reps\n", weight[i], exercises[i], reps[i]);
                        else
                            theWorkout += string.Format("{0}# {1}\n", weight[i], exercises[i]);
                    }

                    if (reps[i] != "" && weight[i] == "")
                    {
                        theWorkout += string.Format("{0}, {1} reps\n", exercises[i], reps[i]);
                    }

                    if (weight[i] == "" && reps[i] == "" && dist[i] == "")
                        theWorkout += string.Format("{0}\n", exercises[i]);
                }
                #endregion

                return theWorkout;
            }
        }

        private void ShowAllWorkouts2(string workout)
        {
            ScrollViewer sv = new ScrollViewer();
            sv.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            sv.HorizontalAlignment = HorizontalAlignment.Left;
            sv.Height = Double.NaN;
            sv.Background = Brushes.Transparent;            
            sv.Margin = new Thickness(0, 20, 0, 10);
            DockPanel.SetDock(sv, Dock.Left);

            WrapPanel wp = new WrapPanel();
            wp.Width = Double.NaN;
            wp.Height = Double.NaN;
            wp.Orientation = Orientation.Vertical;
            wp.Background = new SolidColorBrush(Color.FromRgb(224,224,224));
            wp.HorizontalAlignment = HorizontalAlignment.Left;
            wp.Margin = new Thickness(30, 0, 0, 0);

            sv.Content = wp;
            historyDockPanel.Children.Add(sv);
            historyScrollViewer = sv;
            historyWrapPanel = wp;

            using (OleDbConnection conn = new OleDbConnection(excelDBSource))
            {
                conn.Open();

                if (workout == "" || workout == " ")
                {
                    foreach (DataRow row in workoutDataTable.Rows)
                    {
                        if (row["WorkoutName"].ToString() == " ")
                        { }
                        else
                        {
                            string workoutScore = string.Format("SELECT * " +
                                                               "FROM [WorkoutScore$] WHERE WorkoutName_ID = {0}", row["Id"].ToString());
                            DataSet data = new DataSet();
                            OleDbDataAdapter wScore = new OleDbDataAdapter(workoutScore, conn);
                            DataTable wScoreDataTable = new DataTable();
                            wScore.Fill(wScoreDataTable);
                            data.Tables.Add(wScoreDataTable);
                            if (wScoreDataTable.Rows.Count == 0)
                            {

                            }
                            else
                            {
                                StackPanel sp = new StackPanel();

                                #region Border
                                Border b = new Border();
                                b.BorderBrush = Brushes.Black;
                                b.Background = new SolidColorBrush(Color.FromRgb(224,224,224));
                                b.BorderThickness = new Thickness(0, 0, 0, 1);
                                b.Padding = new Thickness(5, 0, 5, 10);
                                b.Margin = new Thickness(0, 0, 0, 10);
                                #endregion

                                #region textBlock
                                TextBlock tb = new TextBlock();
                                tb.HorizontalAlignment = HorizontalAlignment.Left;
                                tb.VerticalAlignment = VerticalAlignment.Top;
                                tb.MinWidth = 100;
                                tb.MaxWidth = 120;
                                tb.TextWrapping = TextWrapping.Wrap;
                                tb.Width = Double.NaN;
                                tb.Height = Double.NaN;
                                tb.FontFamily = new FontFamily("Verdana");
                                tb.FontSize = 10;
                                tb.Background = Brushes.Transparent;
                                tb.Visibility = Visibility.Visible;
                                tb.Padding = new Thickness(10, 0, 10, 0);
                                tb.Margin = new Thickness(0, 5, 0, 0);
                                tb.Text = DisplayWorkout(row["Id"].ToString());
                                #endregion

                                #region ListView
                                ListView lv = new ListView();
                                lv.HorizontalAlignment = HorizontalAlignment.Left;
                                lv.VerticalAlignment = VerticalAlignment.Top;
                                lv.MinWidth = 100;
                                lv.Width = Double.NaN;
                                lv.Height = Double.NaN;
                                lv.IsHitTestVisible = false;
                                lv.FontFamily = new FontFamily("Verdana");
                                lv.FontSize = 10;
                                lv.Foreground = Brushes.Black;
                                lv.Background = Brushes.White;
                                lv.Padding = new Thickness(0);
                                lv.Margin = new Thickness(5, 1, 5, 0);
                                lv.BorderBrush = Brushes.Black;
                                lv.BorderThickness = new Thickness(1);
                                lv.Visibility = Visibility.Visible;
                                #endregion

                                #region GridView
                                GridView myGridView = new GridView();
                                myGridView.AllowsColumnReorder = true;
                                myGridView.ColumnHeaderToolTip = "test";                                

                                GridViewColumn date = new GridViewColumn();
                                date.Header = "Date";
                                date.DisplayMemberBinding = new Binding("WorkoutDate");
                                date.DisplayMemberBinding.StringFormat = "{0:dd MMM yy}";
                                date.Width = Double.NaN;
                                myGridView.Columns.Add(date);                                

                                GridViewColumn gScore = new GridViewColumn();
                                gScore.Header = "Score";
                                if (data.Tables[0].Rows[0].ItemArray[3].ToString() != "")
                                {
                                    gScore.DisplayMemberBinding = new Binding("WorkoutScoreTime") { Converter = new Converters() };
                                }
                                if (data.Tables[0].Rows[0].ItemArray[4].ToString() != "")
                                    gScore.DisplayMemberBinding = new Binding("WorkoutScoreReps");
                                if (data.Tables[0].Rows[0].ItemArray[5].ToString() != "")
                                    gScore.DisplayMemberBinding = new Binding("WorkoutScoreWeight");
                                gScore.Width = 60;
                                myGridView.Columns.Add(gScore);
                                #endregion

                                lv.View = myGridView;
                                lv.ItemsSource = data.Tables[0].DefaultView;

                                b.Child = sp;
                                sp.Children.Add(tb);
                                sp.Children.Add(lv);
                                historyWrapPanel.Children.Add(b);
                            }
                        }
                    }
                }
                else
                {
                    string workoutScore = string.Format("SELECT * " +
                                                               "FROM [WorkoutScore$] WHERE WorkoutName_ID = {0}", workoutNameSelectedValue);
                    DataSet data = new DataSet();
                    OleDbDataAdapter wScore = new OleDbDataAdapter(workoutScore, conn);
                    DataTable wScoreDataTable = new DataTable();
                    wScore.Fill(wScoreDataTable);
                    data.Tables.Add(wScoreDataTable);
                    if (wScoreDataTable.Rows.Count == 0)
                    {

                    }
                    else
                    {
                        StackPanel sp = new StackPanel();
                        wp.Background = Brushes.Transparent;

                        #region Border
                        Border b = new Border();
                        b.BorderBrush = Brushes.Black;
                        b.Background = new SolidColorBrush(Color.FromRgb(224, 224, 224));
                        b.BorderThickness = new Thickness(0, 0, 0, 1);
                        b.Padding = new Thickness(5, 0, 5, 10);
                        b.Margin = new Thickness(0, 0, 0, 10);
                        #endregion

                        #region textBlock
                        TextBlock tb = new TextBlock();
                        tb.HorizontalAlignment = HorizontalAlignment.Left;
                        tb.VerticalAlignment = VerticalAlignment.Top;
                        tb.MinWidth = 100;
                        tb.MaxWidth = 120;
                        tb.TextWrapping = TextWrapping.Wrap;
                        tb.Width = Double.NaN;
                        tb.Height = Double.NaN;
                        tb.FontFamily = new FontFamily("Verdana");                        
                        tb.FontSize = 10;
                        tb.Background = Brushes.Transparent;
                        tb.Visibility = Visibility.Visible;
                        tb.Padding = new Thickness(10, 0, 10, 0);
                        tb.Margin = new Thickness(0, 5, 0, 0);
                        tb.Text = DisplayWorkout(workoutNameSelectedValue);
                        #endregion

                        #region ListView
                        ListView lv = new ListView();
                        lv.HorizontalAlignment = HorizontalAlignment.Left;
                        lv.VerticalAlignment = VerticalAlignment.Top;
                        lv.MinWidth = 100;
                        lv.Width = Double.NaN;
                        lv.Height = Double.NaN;
                        lv.IsHitTestVisible = false;
                        lv.FontFamily = new FontFamily("Verdana");
                        lv.FontSize = 10;
                        lv.Foreground = Brushes.Black;
                        lv.Background = Brushes.White;
                        lv.Padding = new Thickness(0);
                        lv.Margin = new Thickness(5, 1, 5, 0);
                        lv.BorderBrush = Brushes.Black;
                        lv.BorderThickness = new Thickness(1);
                        lv.Visibility = Visibility.Visible;
                        #endregion

                        #region GridView
                        GridView myGridView = new GridView();
                        myGridView.AllowsColumnReorder = true;
                        myGridView.ColumnHeaderToolTip = "test";

                        GridViewColumn date = new GridViewColumn();
                        date.Header = "Date";
                        date.DisplayMemberBinding = new Binding("WorkoutDate");
                        date.DisplayMemberBinding.StringFormat = "{0:dd MMM yy}";
                        //date.HeaderStringFormat = "'ddMMMyy'";
                        date.Width = Double.NaN;
                        myGridView.Columns.Add(date);

                        GridViewColumn gScore = new GridViewColumn();
                        gScore.Header = "Score";
                        if (data.Tables[0].Rows[0].ItemArray[3].ToString() != "")
                            gScore.DisplayMemberBinding = new Binding("WorkoutScoreTime") { Converter = new Converters() };
                        if (data.Tables[0].Rows[0].ItemArray[4].ToString() != "")
                            gScore.DisplayMemberBinding = new Binding("WorkoutScoreReps");
                        if (data.Tables[0].Rows[0].ItemArray[5].ToString() != "")
                            gScore.DisplayMemberBinding = new Binding("WorkoutScoreWeight");
                        gScore.Width = Double.NaN;
                        myGridView.Columns.Add(gScore);
                        #endregion

                        lv.View = myGridView;
                        lv.ItemsSource = data.Tables[0].DefaultView;

                        b.Child = sp;
                        sp.Children.Add(tb);
                        sp.Children.Add(lv);
                        historyWrapPanel.Children.Add(b);
                    }
                }
            }
        }
    }
}
