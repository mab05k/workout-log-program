﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WorkoutProgram
{
    /// <summary>
    /// Interaction logic for NewWorkout.xaml
    /// </summary>
    public partial class NewWorkout : Page
    {
        public string excelDBSource;

        #region Lists
        public int ExerciseIndex = 0;
        public int ExerciseNumber = 1;
        public int exerciseComboBoxGridRow = 1;        
        public int exerciseLabelGridRow = 1;
        public int exerciseTextBoxGridRow = 1;

        public List<ComboBox> ExerciseComboBoxes;
        public List<ComboBox> ExerciseTypeComboBoxes;
        
        public List<Label> ExerciseLabels;
        public List<Label> ExerciseTypeLabels;
        public List<Label> WeightliftingRepsLabels;
        public List<Label> GymnasticsRepsLabels;
        public List<Label> WeightLabels;
        public List<Label> DistanceLabels;
               
        public List<TextBox> GymnasticsRepsTextBoxes;
        public List<TextBox> WeightTextBoxes;
        public List<TextBox> DistanceTextBoxes;
        public List<TextBox> WeightliftingRepsTextBoxes;
        public List<TextBox> AMRAPDuration;

        public List<Border> ExerciseBorders;
        #endregion

        public NewWorkout(string ExcelDBSource)
        {
            try
            {
            InitializeComponent();
            excelDBSource = ExcelDBSource;
            using (OleDbConnection conn = new OleDbConnection(excelDBSource))
            {
                conn.Open();
                string select = "SELECT * FROM [WorkoutType$] WHERE Id > 0 ORDER BY WorkoutTypeName DESC";
                OleDbCommand cmd = new OleDbCommand(select, conn);

                OleDbDataReader reader = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);

                comboBoxWorkoutType.ItemsSource = dt.DefaultView;
                comboBoxWorkoutType.DisplayMemberPath = "WorkoutTypeName";
                comboBoxWorkoutType.SelectedValuePath = "Id";
            }

            #region Lists
            List<Label> exerciseLabels = new List<Label>();
            exerciseLabels.Clear();
            ExerciseLabels = exerciseLabels;
            List<Label> exerciseTypeLabels = new List<Label>();
            exerciseTypeLabels.Clear();
            ExerciseTypeLabels = exerciseTypeLabels;
            List<Label> weightliftingRepsLabels = new List<Label>();
            weightliftingRepsLabels.Clear();
            WeightliftingRepsLabels = weightliftingRepsLabels;
            List<Label> gymnasticsRepsLabels = new List<Label>();
            gymnasticsRepsLabels.Clear();
            GymnasticsRepsLabels = gymnasticsRepsLabels;
            List<Label> weightLabels = new List<Label>();
            weightLabels.Clear();
            WeightLabels = weightLabels;
            List<Label> distanceLabels = new List<Label>();
            distanceLabels.Clear();
            DistanceLabels = distanceLabels;

            List<ComboBox> exerciseComboBoxes = new List<ComboBox>();
            exerciseComboBoxes.Clear();
            ExerciseComboBoxes = exerciseComboBoxes;
            List<ComboBox> exerciseTypeComboBoxes = new List<ComboBox>();
            exerciseTypeComboBoxes.Clear();
            ExerciseTypeComboBoxes = exerciseTypeComboBoxes;

            List<TextBox> weightliftingRepsTextBoxes = new List<TextBox>();
            weightliftingRepsTextBoxes.Clear();
            WeightliftingRepsTextBoxes = weightliftingRepsTextBoxes;
            List<TextBox> gymnasticsRepsTextBoxes = new List<TextBox>();
            gymnasticsRepsTextBoxes.Clear();
            GymnasticsRepsTextBoxes = gymnasticsRepsTextBoxes;
            List<TextBox> weightTextBoxes = new List<TextBox>();
            weightTextBoxes.Clear();
            WeightTextBoxes = weightTextBoxes;
            List<TextBox> distanceTextBoxes = new List<TextBox>();
            distanceTextBoxes.Clear();
            DistanceTextBoxes = distanceTextBoxes;
            List<TextBox> amrapDuration = new List<TextBox>();
            amrapDuration.Clear();
            AMRAPDuration = amrapDuration;

            List<Border> exerciseBorders = new List<Border>();
            exerciseBorders.Clear();
            ExerciseBorders = exerciseBorders;
            #endregion

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Critical Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(3);
            }
        }
        
        private void OnWorkoutNameGotFocus(object sender, RoutedEventArgs e)
        {
            if (textBoxNewWorkout.Text == "New Workout")
                textBoxNewWorkout.Text = "";            
        }
        private void OnWorkoutTypeDropdownClosed(object sender, EventArgs e)
        {
            ClearLists();

            if (ExerciseTypeComboBoxes.Count == 0)
            {
                if (comboBoxWorkoutType.Text != "")
                    CreateNewExercise(ExerciseIndex);
            }

            if (comboBoxWorkoutType.Text == "AMRAP")
            {
                TextBox AMRAPTextBox = new TextBox();
                ExercisesGrid.Children.Add(AMRAPTextBox);
                AMRAPTextBox.SetValue(Grid.RowProperty, 0);
                AMRAPTextBox.SetValue(Grid.ColumnProperty, 1);
                AMRAPTextBox.HorizontalAlignment = HorizontalAlignment.Left;
                AMRAPTextBox.VerticalAlignment = VerticalAlignment.Top;
                AMRAPTextBox.Text = "";
                AMRAPTextBox.Margin = new Thickness(20, 5, 0, 0);
                AMRAPTextBox.Width = 30;
                AMRAPTextBox.Height = 23;
                AMRAPTextBox.Name = "textBoxAMRAPDuration";
                AMRAPDuration.Add(AMRAPTextBox);

                Label AMRAPLabel = new Label();
                ExercisesGrid.Children.Add(AMRAPLabel);
                AMRAPLabel.SetValue(Grid.RowProperty, 0);
                AMRAPLabel.SetValue(Grid.ColumnProperty, 1);
                AMRAPLabel.Content = "Duration";
                AMRAPLabel.HorizontalAlignment = HorizontalAlignment.Left;
                AMRAPLabel.VerticalAlignment = VerticalAlignment.Top;
                AMRAPLabel.Margin = new Thickness(50, 5, 0, 0);
                AMRAPLabel.FontSize = 10;
                AMRAPLabel.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
            }
        }      

        private void Exercise_DropdownClosed(object sender, EventArgs e, int exerciseNumber)
        {
            #region Hide labels, comboboxes, & textboxes
            // Hide labels
            ExerciseTypeLabels[exerciseNumber].Visibility = Visibility.Collapsed;
            WeightliftingRepsLabels[exerciseNumber].Visibility = Visibility.Collapsed;
            GymnasticsRepsLabels[exerciseNumber].Visibility = Visibility.Collapsed;
            WeightLabels[exerciseNumber].Visibility = Visibility.Collapsed;
            DistanceLabels[exerciseNumber].Visibility = Visibility.Collapsed;

            // Hide combobox
            ExerciseTypeComboBoxes[exerciseNumber].Visibility = Visibility.Collapsed;

            // Hide textboxes
            GymnasticsRepsTextBoxes[exerciseNumber].Visibility = Visibility.Collapsed;
            WeightTextBoxes[exerciseNumber].Visibility = Visibility.Collapsed;
            DistanceTextBoxes[exerciseNumber].Visibility = Visibility.Collapsed;
            WeightliftingRepsTextBoxes[exerciseNumber].Visibility = Visibility.Collapsed;
            #endregion

            // pulls up exercise options comboboxes based on selection of exercise types
            #region switch
            switch (ExerciseComboBoxes[exerciseNumber].Text)
            {
                case "Weightlifting":
                    ExerciseTypeLabels[exerciseNumber].Visibility = Visibility.Visible;
                    ExerciseTypeLabels[exerciseNumber].Content = "Weightlifting";
                    ExerciseTypeComboBoxes[exerciseNumber].Visibility = Visibility.Visible;
                                       
                    if (comboBoxWorkoutType.Text != "21-15-9" && comboBoxWorkoutType.Text != "30 Reps for time")
                    {
                        WeightliftingRepsTextBoxes[exerciseNumber].Visibility = Visibility.Visible;
                        WeightliftingRepsLabels[exerciseNumber].Visibility = Visibility.Visible;
                    }

                    if (comboBoxWorkoutType.Text != "Max Effort")
                    {
                        WeightLabels[exerciseNumber].Visibility = Visibility.Visible;
                        WeightTextBoxes[exerciseNumber].Visibility = Visibility.Visible;
                    }

                    using (OleDbConnection conn = new OleDbConnection(excelDBSource))
                    {
                        conn.Open();
                        string select = "SELECT * FROM [ExerciseType$] WHERE ExerciseStyle_ID = 2 ORDER BY ExerciseTypeName";
                        OleDbCommand cmd = new OleDbCommand(select, conn);
                        OleDbDataReader reader = cmd.ExecuteReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        ExerciseTypeComboBoxes[exerciseNumber].ItemsSource = dt.DefaultView;
                        ExerciseTypeComboBoxes[exerciseNumber].DisplayMemberPath = "ExerciseTypeName";
                        ExerciseTypeComboBoxes[exerciseNumber].SelectedValuePath = "Id";
                    }
                    break;
                case "Gymnastics":
                    ExerciseTypeLabels[exerciseNumber].Visibility = Visibility.Visible;
                    ExerciseTypeLabels[exerciseNumber].Content = "Gymnastics";
                    ExerciseTypeComboBoxes[exerciseNumber].Visibility = Visibility.Visible;

                    if (comboBoxWorkoutType.Text != "21-15-9")
                    {
                        GymnasticsRepsTextBoxes[exerciseNumber].Visibility = Visibility.Visible;
                        GymnasticsRepsLabels[exerciseNumber].Visibility = Visibility.Visible;
                    }

                    using (OleDbConnection conn = new OleDbConnection(excelDBSource))
                    {
                        conn.Open();
                        string select = "SELECT * FROM [ExerciseType$] WHERE ExerciseStyle_ID = 3 ORDER BY ExerciseTypeName";
                        OleDbCommand cmd = new OleDbCommand(select, conn);
                        OleDbDataReader reader = cmd.ExecuteReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        ExerciseTypeComboBoxes[exerciseNumber].ItemsSource = dt.DefaultView;
                        ExerciseTypeComboBoxes[exerciseNumber].DisplayMemberPath = "ExerciseTypeName";
                        ExerciseTypeComboBoxes[exerciseNumber].SelectedValuePath = "Id";
                    }
                    break;
                case "Cardio":
                    ExerciseTypeLabels[exerciseNumber].Visibility = Visibility.Visible;
                    ExerciseTypeLabels[exerciseNumber].Content = "Cardio";
                    DistanceLabels[exerciseNumber].Visibility = Visibility.Visible;
                    ExerciseTypeComboBoxes[exerciseNumber].Visibility = Visibility.Visible;
                    DistanceTextBoxes[exerciseNumber].Visibility = Visibility.Visible;

                    using (OleDbConnection conn = new OleDbConnection(excelDBSource))
                    {
                        conn.Open();
                        string select = "SELECT * FROM [ExerciseType$] WHERE ExerciseStyle_ID = 4 ORDER BY ExerciseTypeName";
                        OleDbCommand cmd = new OleDbCommand(select, conn);
                        OleDbDataReader reader = cmd.ExecuteReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        ExerciseTypeComboBoxes[exerciseNumber].ItemsSource = dt.DefaultView;
                        ExerciseTypeComboBoxes[exerciseNumber].DisplayMemberPath = "ExerciseTypeName";
                        ExerciseTypeComboBoxes[exerciseNumber].SelectedValuePath = "Id";
                    }
                    break;
                default:
                    break;
            }
            #endregion
        }
        private void ExerciseType_DropdownClosed(object sender, EventArgs e, int exerciseNumber)
        {
            if (comboBoxWorkoutType.Text != "Max Effort" && comboBoxWorkoutType.Text != "30 Reps for time")
            {
                if (exerciseNumber >= ExerciseNumber - 2)            
                CreateNewExercise(ExerciseIndex);  
            }                      
        }
        private void CreateNewExercise(int exerciseIndex)
        {
            int exerciseLabelGridColumn = 0;
            int exerciseComboBoxGridColumn = 1;
            int exerciseTextBoxGridColumn = 3;
            
            #region ComboBoxes
            ComboBox ExerciseComboBox = new ComboBox();
            ExercisesGrid.Children.Add(ExerciseComboBox);
            ExerciseComboBox.SetValue(Grid.RowProperty, exerciseComboBoxGridRow);
            ExerciseComboBox.SetValue(Grid.ColumnProperty, exerciseComboBoxGridColumn);
            ExerciseComboBox.HorizontalAlignment = HorizontalAlignment.Left;
            ExerciseComboBox.VerticalAlignment = VerticalAlignment.Top;
            ExerciseComboBox.Width = 120;
            ExerciseComboBox.Visibility = Visibility.Visible;
            ExerciseComboBox.Margin = new Thickness(6, 20, 5, 10);
            ExerciseComboBox.DropDownClosed += (sender, e) => Exercise_DropdownClosed(sender, e, exerciseIndex);

            using (OleDbConnection conn = new OleDbConnection(excelDBSource))
            {
                conn.Open();
                string select = "SELECT * FROM [ExerciseStyle$]";
                OleDbCommand cmd = new OleDbCommand(select, conn);
                OleDbDataReader reader = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);

                ExerciseComboBox.ItemsSource = dt.DefaultView;
                ExerciseComboBox.DisplayMemberPath = "ExerciseStyleName";
                ExerciseComboBox.SelectedValuePath = "Id";
            }

            ExerciseComboBoxes.Add(ExerciseComboBox);
            exerciseComboBoxGridColumn++;

            ComboBox ExerciseTypeComboBox = new ComboBox();
            ExercisesGrid.Children.Add(ExerciseTypeComboBox);
            ExerciseTypeComboBox.SetValue(Grid.RowProperty, exerciseComboBoxGridRow);
            ExerciseTypeComboBox.SetValue(Grid.ColumnProperty, exerciseComboBoxGridColumn);
            ExerciseTypeComboBox.HorizontalAlignment = HorizontalAlignment.Left;
            ExerciseTypeComboBox.VerticalAlignment = VerticalAlignment.Top;
            ExerciseTypeComboBox.Width = 120;
            ExerciseTypeComboBox.Visibility = Visibility.Collapsed;
            ExerciseTypeComboBox.Margin = new Thickness(10, 20, 0, 0);
            ExerciseTypeComboBox.DropDownClosed += (sender, e) => ExerciseType_DropdownClosed(sender, e, exerciseIndex);

            ExerciseTypeComboBoxes.Add(ExerciseTypeComboBox);
            #endregion

            #region Labels

            Label ExerciseLabel = new Label();
            ExercisesGrid.Children.Add(ExerciseLabel);
            ExerciseLabel.SetValue(Grid.RowProperty, exerciseLabelGridRow);
            ExerciseLabel.SetValue(Grid.ColumnProperty, exerciseLabelGridColumn);
            ExerciseLabel.Content = string.Format("Exercise {0}:", ExerciseNumber);
            ExerciseLabel.HorizontalAlignment = HorizontalAlignment.Left;
            ExerciseLabel.VerticalAlignment = VerticalAlignment.Top;
            ExerciseLabel.Visibility = Visibility.Visible;
            ExerciseLabel.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
            ExerciseLabel.Margin = new Thickness(39, 15, 0, 0);

            ExerciseLabels.Add(ExerciseLabel);
            exerciseLabelGridColumn += 2;

            Label ExerciseTypeLabel = new Label();
            ExercisesGrid.Children.Add(ExerciseTypeLabel);
            ExerciseTypeLabel.SetValue(Grid.RowProperty, exerciseLabelGridRow);
            ExerciseTypeLabel.SetValue(Grid.ColumnProperty, exerciseLabelGridColumn);
            ExerciseTypeLabel.Content = ExerciseComboBoxes[ExerciseIndex].Text;
            ExerciseTypeLabel.FontSize = 10;
            ExerciseTypeLabel.HorizontalAlignment = HorizontalAlignment.Left;
            ExerciseTypeLabel.VerticalAlignment = VerticalAlignment.Top;
            ExerciseTypeLabel.Visibility = Visibility.Collapsed;
            ExerciseTypeLabel.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
            ExerciseTypeLabel.Margin = new Thickness(10, 0, 0, 0);

            ExerciseTypeLabels.Add(ExerciseTypeLabel);
            exerciseLabelGridColumn++;            

            Label GymnasticsRepsLabel = new Label();
            ExercisesGrid.Children.Add(GymnasticsRepsLabel);
            GymnasticsRepsLabel.SetValue(Grid.RowProperty, exerciseLabelGridRow);
            GymnasticsRepsLabel.SetValue(Grid.ColumnProperty, exerciseLabelGridColumn);
            GymnasticsRepsLabel.Content = "Reps";
            GymnasticsRepsLabel.FontSize = 10;
            GymnasticsRepsLabel.HorizontalAlignment = HorizontalAlignment.Left;
            GymnasticsRepsLabel.VerticalAlignment = VerticalAlignment.Top;
            GymnasticsRepsLabel.Visibility = Visibility.Collapsed;
            GymnasticsRepsLabel.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
            GymnasticsRepsLabel.Margin = new Thickness(10, 0, 0, 0);

            GymnasticsRepsLabels.Add(GymnasticsRepsLabel);

            Label WeightLabel = new Label();
            ExercisesGrid.Children.Add(WeightLabel);
            WeightLabel.SetValue(Grid.RowProperty, exerciseLabelGridRow);
            WeightLabel.SetValue(Grid.ColumnProperty, exerciseLabelGridColumn);
            WeightLabel.Content = "Weight";
            WeightLabel.FontSize = 10;
            WeightLabel.HorizontalAlignment = HorizontalAlignment.Left;
            WeightLabel.VerticalAlignment = VerticalAlignment.Top;
            WeightLabel.Visibility = Visibility.Collapsed;
            WeightLabel.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
            WeightLabel.Margin = new Thickness(10, 0, 0, 0);

            WeightLabels.Add(WeightLabel);

            Label DistanceLabel = new Label();
            ExercisesGrid.Children.Add(DistanceLabel);
            DistanceLabel.SetValue(Grid.RowProperty, exerciseLabelGridRow);
            DistanceLabel.SetValue(Grid.ColumnProperty, exerciseLabelGridColumn);
            DistanceLabel.Content = "Dist";
            DistanceLabel.FontSize = 10;
            DistanceLabel.HorizontalAlignment = HorizontalAlignment.Left;
            DistanceLabel.VerticalAlignment = VerticalAlignment.Top;
            DistanceLabel.Visibility = Visibility.Collapsed;
            DistanceLabel.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
            DistanceLabel.Margin = new Thickness(10, 0, 0, 0);

            DistanceLabels.Add(DistanceLabel);
            exerciseLabelGridColumn++;

            Label WeightliftingRepsLabel = new Label();
            ExercisesGrid.Children.Add(WeightliftingRepsLabel);
            WeightliftingRepsLabel.SetValue(Grid.RowProperty, exerciseLabelGridRow);
            WeightliftingRepsLabel.SetValue(Grid.ColumnProperty, exerciseLabelGridColumn);
            WeightliftingRepsLabel.Content = "Reps";
            WeightliftingRepsLabel.FontSize = 10;
            WeightliftingRepsLabel.HorizontalAlignment = HorizontalAlignment.Left;
            WeightliftingRepsLabel.VerticalAlignment = VerticalAlignment.Top;
            WeightliftingRepsLabel.Visibility = Visibility.Collapsed;
            WeightliftingRepsLabel.Foreground = new SolidColorBrush(Color.FromRgb(224, 224, 224));
            WeightliftingRepsLabel.Margin = new Thickness(10, 0, 0, 0);

            WeightliftingRepsLabels.Add(WeightliftingRepsLabel);        
            #endregion

            #region TextBoxes

            TextBox GymnasticsRepsTextBox = new TextBox();
            ExercisesGrid.Children.Add(GymnasticsRepsTextBox);
            GymnasticsRepsTextBox.SetValue(Grid.RowProperty, exerciseTextBoxGridRow);
            GymnasticsRepsTextBox.SetValue(Grid.ColumnProperty, exerciseTextBoxGridColumn);
            GymnasticsRepsTextBox.HorizontalAlignment = HorizontalAlignment.Left;
            GymnasticsRepsTextBox.VerticalAlignment = VerticalAlignment.Top;
            GymnasticsRepsTextBox.Height = 23;
            GymnasticsRepsTextBox.Width = 40;
            GymnasticsRepsTextBox.Visibility = Visibility.Collapsed;
            GymnasticsRepsTextBox.Margin = new Thickness(10, 20, 10, 0);

            GymnasticsRepsTextBoxes.Add(GymnasticsRepsTextBox);

            TextBox WeightTextBox = new TextBox();
            ExercisesGrid.Children.Add(WeightTextBox);
            WeightTextBox.SetValue(Grid.RowProperty, exerciseTextBoxGridRow);
            WeightTextBox.SetValue(Grid.ColumnProperty, exerciseTextBoxGridColumn);
            WeightTextBox.HorizontalAlignment = HorizontalAlignment.Left;
            WeightTextBox.VerticalAlignment = VerticalAlignment.Top;
            WeightTextBox.Height = 23;
            WeightTextBox.Width = 55;
            WeightTextBox.Visibility = Visibility.Collapsed;
            WeightTextBox.Margin = new Thickness(10, 20, 10, 0);

            WeightTextBoxes.Add(WeightTextBox);

            TextBox DistanceTextBox = new TextBox();
            ExercisesGrid.Children.Add(DistanceTextBox);
            DistanceTextBox.SetValue(Grid.RowProperty, exerciseTextBoxGridRow);
            DistanceTextBox.SetValue(Grid.ColumnProperty, exerciseTextBoxGridColumn);
            DistanceTextBox.HorizontalAlignment = HorizontalAlignment.Left;
            DistanceTextBox.VerticalAlignment = VerticalAlignment.Top;
            DistanceTextBox.Height = 23;
            DistanceTextBox.Visibility = Visibility.Collapsed;
            DistanceTextBox.Margin = new Thickness(10, 20, 10, 0);

            DistanceTextBoxes.Add(DistanceTextBox);
            exerciseTextBoxGridColumn++;

            TextBox WeightliftingRepsTextBox = new TextBox();
            ExercisesGrid.Children.Add(WeightliftingRepsTextBox);
            WeightliftingRepsTextBox.SetValue(Grid.RowProperty, exerciseTextBoxGridRow);
            WeightliftingRepsTextBox.SetValue(Grid.ColumnProperty, exerciseTextBoxGridColumn);
            WeightliftingRepsTextBox.HorizontalAlignment = HorizontalAlignment.Left;
            WeightliftingRepsTextBox.VerticalAlignment = VerticalAlignment.Top;
            WeightliftingRepsTextBox.Height = 23;
            WeightliftingRepsTextBox.Width = 40;
            WeightliftingRepsTextBox.Visibility = Visibility.Collapsed;
            WeightliftingRepsTextBox.Margin = new Thickness(10, 20, 10, 0);

            WeightliftingRepsTextBoxes.Add(WeightliftingRepsTextBox);

            #endregion            

            exerciseComboBoxGridRow++;
            exerciseLabelGridRow++;
            exerciseTextBoxGridRow++;
            ExerciseNumber++;
            ExerciseIndex++;
        }

        private string DisplayWorkout() // displays the workout in the listbox
        {
            ClearFields();

            string theWorkout = null;
            theWorkout += "\"" + textBoxNewWorkout.Text + "\"\n";

            if (comboBoxWorkoutType.Text == "AMRAP")
            {
                theWorkout += (comboBoxWorkoutType.Text + " " + AMRAPDuration[0].Text + " Minutes:\n");
                theWorkout += AddExercises();
            }
            else
            {
                theWorkout += comboBoxWorkoutType.Text + ":\n";
                theWorkout += AddExercises();
            }
            return theWorkout;
        }
        private string AddExercises()
        {
            string theWorkout = null;
            try
            {
                for (int i = 0; i < ExerciseTypeComboBoxes.Count; i++)
                {
                    #region Error Catching
                    if (GymnasticsRepsTextBoxes[i].Text != "")
                        int.Parse(GymnasticsRepsTextBoxes[i].Text);
                    if (WeightTextBoxes[i].Text != "")
                        int.Parse(WeightTextBoxes[i].Text);
                    if (DistanceTextBoxes[i].Text != "")
                        int.Parse(DistanceTextBoxes[i].Text);
                    if (WeightliftingRepsTextBoxes[i].Text != "")
                        int.Parse(WeightliftingRepsTextBoxes[i].Text);
                    #endregion

                    #region Add Exercises
                    if (ExerciseComboBoxes[i].Text == "Weightlifting")
                    {
                        if (WeightliftingRepsTextBoxes[i].Visibility == Visibility.Collapsed)
                        {
                            theWorkout += string.Format("{0}# {1}\n", WeightTextBoxes[i].Text, ExerciseTypeComboBoxes[i].Text);
                        }
                        else
                        {
                            if (WeightTextBoxes[i].Visibility == Visibility.Collapsed)
                            {
                                if (WeightliftingRepsTextBoxes[i].Text == "1" || WeightliftingRepsTextBoxes[i].Text == "3" || WeightliftingRepsTextBoxes[i].Text == "5")
                                {
                                    theWorkout += string.Format("{0} RM\n{1}", WeightliftingRepsTextBoxes[i].Text, ExerciseTypeComboBoxes[i].Text);
                                }
                                else
                                {
                                    theWorkout += string.Format("{0} reps\n{1}", WeightliftingRepsTextBoxes[i].Text, ExerciseTypeComboBoxes[i].Text);
                                }
                            }
                            else
                            {
                                theWorkout += string.Format("{0}# {1}, {2} reps\n", WeightTextBoxes[i].Text, ExerciseTypeComboBoxes[i].Text, WeightliftingRepsTextBoxes[i].Text);
                            }
                        }
                    }
                    if (ExerciseComboBoxes[i].Text == "Gymnastics")
                    {
                        if (GymnasticsRepsTextBoxes[i].Visibility == Visibility.Collapsed)
                        {
                            theWorkout += string.Format("{0}\n", ExerciseTypeComboBoxes[i].Text);
                        }
                        else
                        {
                            theWorkout += string.Format("{0}, {1} reps\n", ExerciseTypeComboBoxes[i].Text, GymnasticsRepsTextBoxes[i].Text);
                        }
                    }
                    if (ExerciseComboBoxes[i].Text == "Cardio")
                    {
                        theWorkout += string.Format("{0} {1}m\n", ExerciseTypeComboBoxes[i].Text, DistanceTextBoxes[i].Text);
                    }
                    #endregion
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Weight, Rep and Distance inputs must be numbers.", "Invalid Input", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return theWorkout;
        }
        
        private void OnButtonDisplay_Click(object sender, RoutedEventArgs e)
        {
            string displayWorkout = DisplayWorkout();
            textBlockDisplayWorkout.Visibility = Visibility.Visible;
            textBlockDisplayWorkout.Text = displayWorkout;
        }
        private void OnButtonAddNewWorkout_Click(object sender, RoutedEventArgs e) // Add new workout to the WorkoutList
        {
            try
            {
                string errorMessage = null;
                int errorCount = 0;

                #region Errors
                #region basic options errors
                if (textBoxNewWorkout.Text == "New Workout")
                {
                    errorMessage += "Must input workout name.\n";
                    errorCount++;
                }
                if (comboBoxWorkoutType.Text == "")
                {
                    errorMessage += "Workout type not selected.\n";
                    errorCount++;
                }
                if (comboBoxWorkoutType.Text == "AMRAP" && AMRAPDuration[0].Text == "")
                {
                    errorMessage += "AMRAP duration not specified.\n";
                    errorCount++;
                }
                #endregion

                for (int i = 0; i < ExerciseComboBoxes.Count; i++)
                {
                    #region Basic Exercise Errors
                    if (ExerciseComboBoxes[0].Text == "")
                    {
                        errorMessage += "You must select at least one type of workout.\n";
                        errorCount++;
                    }
                    if (ExerciseComboBoxes[i].Text != "")
                    {
                        if (ExerciseTypeComboBoxes[i].Text == "")
                        {
                            errorMessage += string.Format("Exercise {0}: You must select an exercise.\n", i + 1);
                            errorCount++;
                        }
                    }
                    #endregion
                    if (ExerciseTypeComboBoxes[i].Text != "")
                    {
                        #region Weightlifting
                        if (ExerciseComboBoxes[i].Text == "Weightlifting")
                        {
                            if (WeightTextBoxes[i].Visibility == Visibility.Visible && WeightTextBoxes[i].Text == "")
                            {
                                errorMessage += string.Format("Exercise {0}: You must input a weight for {1}.\n", i + 1, ExerciseTypeComboBoxes[i].Text);
                                errorCount++;
                            }
                            if (WeightliftingRepsTextBoxes[i].Visibility == Visibility.Visible && WeightliftingRepsTextBoxes[i].Text == "")
                            {
                                errorMessage += string.Format("Exercise {0}: You must input reps for {1}.\n", i + 1, ExerciseTypeComboBoxes[i].Text);
                                errorCount++;
                            }
                        }
                        #endregion
                        #region Gymnastics
                        if (ExerciseComboBoxes[i].Text == "Gymnastics")
                        {
                            if (GymnasticsRepsTextBoxes[i].Visibility == Visibility.Visible && GymnasticsRepsTextBoxes[i].Text == "")
                            {
                                errorMessage += string.Format("Exercise {0}: You must input reps for {1}.\n", i + 1, ExerciseTypeComboBoxes[i].Text);
                                errorCount++;
                            }
                        }
                        #endregion
                        #region Cardio
                        if (ExerciseComboBoxes[i].Text == "Cardio")
                        {
                            if (DistanceTextBoxes[i].Visibility == Visibility.Visible && DistanceTextBoxes[i].Text == "")
                            {
                                errorMessage += string.Format("Exercise {0}: You must input distance for {1}.\n", i + 1, ExerciseTypeComboBoxes[i].Text);
                                errorCount++;
                            }
                        }
                        #endregion
                    }
                }
                #endregion

                #region DB add workout
                if (errorCount == 0)
                {
                    using (OleDbConnection conn = new OleDbConnection(excelDBSource))
                    {
                        conn.Open();

                        string select = "SELECT * FROM [WorkoutName$] WHERE Id > 0 ORDER BY WorkoutName";
                        OleDbCommand cmd = new OleDbCommand(select, conn);

                        OleDbDataReader reader = cmd.ExecuteReader();
                        DataTable dt = new DataTable();
                        dt.Load(reader);
                        int workoutNumber = dt.Rows.Count;

                        string addWorkoutName = string.Format(@"INSERT INTO [WorkoutName$] (Id, WorkoutName, WorkoutType_ID, amrapDuration)
                    VALUES(@Id, @workoutName, @workoutTypeInt, @amrapDurationInt)");
                        string addWorkoutExercises = string.Format(@"INSERT INTO [ExerciseWeightRepDist$](ExerciseType_ID, ExerciseWeight, ExerciseReps, ExerciseDist, WorkoutName_ID)
                    VALUES(@exerciseType_ID, @exerciseWeight, @exerciseReps, @exerciseDist, @workoutName_ID)");

                        #region Fill WorkoutName Table
                        using (OleDbCommand workoutTypeCommand = new OleDbCommand(addWorkoutName, conn))
                        {
                            workoutTypeCommand.Parameters.Add("Id", OleDbType.Integer).Value = workoutNumber + 1;
                            workoutTypeCommand.Parameters.Add("@workoutName", OleDbType.Char).Value = textBoxNewWorkout.Text;
                            workoutTypeCommand.Parameters.Add("@workoutTypeInt", OleDbType.Integer).Value = comboBoxWorkoutType.SelectedValue;

                            if (AMRAPDuration[0].Text == "")
                                workoutTypeCommand.Parameters.Add("@amrapDurationInt", OleDbType.Integer).Value = DBNull.Value;
                            else
                                workoutTypeCommand.Parameters.Add("@amrapDurationInt", OleDbType.Integer).Value = int.Parse(AMRAPDuration[0].Text);

                            int workoutTypeInfo = workoutTypeCommand.ExecuteNonQuery();
                        }

                        #endregion
                        string getWorkoutNameId = GetWorkoutNameId(textBoxNewWorkout.Text);

                        #region Exercises
                        for (int i = 0; i < ExerciseTypeComboBoxes.Count; i++)
                        {
                            if (ExerciseComboBoxes[i].Text != "")
                            {
                                using (OleDbCommand addExercise = new OleDbCommand(addWorkoutExercises, conn))
                                {
                                    addExercise.Parameters.Add("@exerciseType_ID", OleDbType.Integer).Value = ExerciseTypeComboBoxes[i].SelectedValue;

                                    if (WeightTextBoxes[i].Text == "")
                                        addExercise.Parameters.Add("@exerciseWeight", OleDbType.Integer).Value = DBNull.Value;
                                    else
                                        addExercise.Parameters.Add("@exerciseWeight", OleDbType.Integer).Value = int.Parse(WeightTextBoxes[i].Text);

                                    if (GymnasticsRepsTextBoxes[i].Text == "" && WeightliftingRepsTextBoxes[i].Text == "")
                                        addExercise.Parameters.Add("@exerciseReps", OleDbType.Integer).Value = DBNull.Value;
                                    if (WeightliftingRepsTextBoxes[i].Text != "")
                                        addExercise.Parameters.Add("@exerciseReps", OleDbType.Integer).Value = int.Parse(WeightliftingRepsTextBoxes[i].Text);
                                    if (GymnasticsRepsTextBoxes[i].Text != "")
                                        addExercise.Parameters.Add("@exerciseReps", OleDbType.Integer).Value = int.Parse(GymnasticsRepsTextBoxes[i].Text);

                                    if (DistanceTextBoxes[i].Text == "")
                                        addExercise.Parameters.Add("@exerciseDist", OleDbType.Integer).Value = DBNull.Value;
                                    else
                                        addExercise.Parameters.Add("@exerciseDist", OleDbType.Integer).Value = int.Parse(DistanceTextBoxes[i].Text);

                                    addExercise.Parameters.Add("@workoutName_ID", OleDbType.Integer).Value = int.Parse(getWorkoutNameId);

                                    int exerciseInfo = addExercise.ExecuteNonQuery();
                                }
                            }
                        }

                        MessageBox.Show("New workout added:\n\n" + DisplayWorkout());

                        #endregion
                    }
                    this.NavigationService.Navigate(new NewWorkout(excelDBSource));
                }

                #endregion

                if (errorCount != 0)
                {
                    MessageBox.Show(errorCount + " Error(s):\n\n" + errorMessage, "Missing Information", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Weight, Rep and Distance inputs must be numbers.", "Invalid Input", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        
        private void ClearFields()
        {
            textBlockDisplayWorkout.Text = "";            
        }
        private void ClearLists()
        {
            ExercisesGrid.Children.Clear();

            exerciseComboBoxGridRow = 1;
            exerciseLabelGridRow = 1;
            exerciseTextBoxGridRow = 1;
            ExerciseNumber = 1;
            ExerciseIndex = 0;

            ExerciseComboBoxes.Clear();
            ExerciseTypeComboBoxes.Clear();
        
            ExerciseLabels.Clear();
            ExerciseTypeLabels.Clear();
            WeightliftingRepsLabels.Clear();
            GymnasticsRepsLabels.Clear();
            WeightLabels.Clear();
            DistanceLabels.Clear();
               
            GymnasticsRepsTextBoxes.Clear();
            WeightTextBoxes.Clear();
            DistanceTextBoxes.Clear();
            WeightliftingRepsTextBoxes.Clear();
            AMRAPDuration.Clear();
        }
        private string GetWorkoutNameId(string workoutName)
        {
            string getWorkoutNameId = string.Format(@"SELECT Id FROM [WorkoutName$]
                    WHERE WorkoutName = @workoutNameId");

            using (OleDbConnection conn = new OleDbConnection(excelDBSource))
            {
                conn.Open();
                using (OleDbCommand wNameId = new OleDbCommand(getWorkoutNameId, conn))
                {
                    wNameId.Parameters.Add("@workoutNameId", OleDbType.Char).Value = workoutName;
                    using (OleDbDataReader reader = wNameId.ExecuteReader())
                    {
                        string id = null;
                        while (reader.Read())
                        {
                            id = reader[0].ToString();
                        }
                        return id;
                    }
                }
            }
        }         
    }
}
