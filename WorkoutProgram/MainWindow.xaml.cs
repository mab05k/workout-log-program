﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WorkoutProgram
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string excelDBSource = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;" +
                                                    @"Data Source=|DataDirectory|\WorkoutDatabaseXL.xlsx;" +
                                                    "Extended Properties=\"Excel 12.0 Xml;HDR=YES;\"");

        public string excelDBSource2 = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;" +
                                                    @"Data Source=|DataDirectory|\WorkoutDatabaseXL.xls;" +
                                                    "Extended Properties=\"Excel 8.0;HDR=YES;IMEX=2\"");

        public string x = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;" +
                                                    @"Data Source=D:\6. Documents\WorkoutDatabaseXL.xls;" +
                                                    "Extended Properties=\"Excel 8.0;HDR=YES;IMEX=2\"");

        public MainWindow()
        {
            try
            {
                using (OleDbConnection conn = new OleDbConnection(excelDBSource2))
                {
                    conn.Open();
                    string select = "SELECT * FROM [WorkoutName$] WHERE Id > 0 ORDER BY WorkoutName";
                    OleDbCommand cmd = new OleDbCommand(select, conn);

                    OleDbDataReader reader = cmd.ExecuteReader();
                }
                InitializeComponent();
                mainContentFrame.Content = new HomeFrame();
            }
            catch (OleDbException ex)
            {
                MessageBox.Show(ex.ToString(), "Critical Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(3);
            }
        }

        private void OnButtonNewWorkout_Click(object sender, RoutedEventArgs e)
        {
                mainContentFrame.Content = new NewWorkout(excelDBSource2);            
        }

        private void buttonInputWorkoutScore_Click(object sender, RoutedEventArgs e)
        {
            mainContentFrame.Content = new WorkoutScore(excelDBSource2);            
        }

        private void buttonShowHistory_Click(object sender, RoutedEventArgs e)
        {
            mainContentFrame.Content = new WorkoutHistory(excelDBSource2);            
        }
    }
}
